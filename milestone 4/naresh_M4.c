 #include<stdio.h>

 int addNewElement();
 int searchElement(int);
 int removeElement();
 int sortArray();
 int printArray();

 static int a[20] = {12, 5, 56, 25, 10, 6, 9, 78, 2, 45};
 int n = 10, flag, indexA, find;

 int main(){
    int task;
    for(int flag=0; flag!=1;){
        printf(" 1. Add a New Element in the Array \n 2. Search if an Element Exists in the Array \n 3. Remove an Element from the Array \n 4. Sort the Array \n 5. Print all Elements in the Array \n 6. Exit \n Enter the number associated with Task to Perform: (1-6)\n");
        scanf("%d",&task);
        switch (task){
        case 1:
            addNewElement();
            break;
        case 2:
            printf("Enter the Element to Search: \n");
            scanf("%d", &find);
            int res = searchElement(find);
            if (res == 1){
                printf("Found %d at Index %d \n",find, indexA);
            }
            else
                printf("Entered Number %d not found \n", find);
            break;
        case 3:
            removeElement();
            break;
        case 4:
            sortArray();
            break;
        case 5:
            printArray();
            break;
        case 6:
            flag=1;
            break;
        default:
            printf("Invalid Option! Please Try Again. \n");
        }
    }
    return 0;
 }

 int addNewElement(){
    int newElement;
    printf("Enter the New Element to Add in the Array: \n");
    scanf("%d", &newElement);
    if(n!=20){
    if (searchElement(newElement)==0){
        a[n] = newElement;
        printf("Element Added Successfully. \n");
        n++;
    }
    else
        printf("Element already added. Please Try Again. \n");
    }
    else
        printf("Array Full! \n");
 }
 int searchElement(int search){
    for(int i=0; i<n; i++){
        if(search == a[i]){
            flag=1;
            indexA=i;
            break;
        }
        else
            flag=0;
    }
    return flag;
 }

 int removeElement(){
    int remov;
    printf("Enter the Element to Remove: \n");
    scanf("%d",&remov);
    if (searchElement(remov)==1){
        for(int i=0; indexA<n-1; indexA++){
            a[indexA]=a[indexA+1];
        }
        n--;
        printf("Element Removed \n");
    }
    else
        printf("%d not Removed \n",remov);
 }

 int sortArray(){
    int sort;
    printf(" 1. Ascending Order \n 2. Descending Order \n Enter the number associated with the sorting order: \n");
    scanf("%d", &sort);
    if (sort == 1){
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                if (a[i]>a[j]){
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
    }
    else if (sort == 2){
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                if (a[i]<a[j]){
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
    }
    else{
    printf("Please Try Again! \n");
    sortArray();
    }
 }

 int printArray(){
    for(int i=0; i<n; i++){
        printf("%d ",a[i]);
    }
    printf("\n");
 }
