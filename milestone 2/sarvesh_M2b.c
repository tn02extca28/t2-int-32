#include<stdio.h>

int gcd(int, int);

int main(){
    int a, b;
    printf("Enter Two Numbers for GCD: \n");
    scanf("%d %d", &a, &b);
    printf("Greatest Common Divisor is %d", gcd(a, b));
    return 0;
}
int gcd(int a, int b){
    if (b == 0)
        return a;
    else
        return gcd(b, a % b);
}
