#include <stdio.h>

int main(){
    unsigned int number, f;

    printf("Enter a Positive Number to Find Factors: \n");
    scanf("%d", &number);

    printf("Factors of %d are: ", number);
    for(f=1; f<=number; f++){
        if (number%f == 0){
            printf("%d, ",f);
        }
    }
    return 0;
}
