#include<stdio.h>

double power(double, int);

double PV(double, unsigned int, double);

int main(){
    double r, fv;
    unsigned int n;
    printf("Enter the Future Value, Rate and Period: \n");
    scanf("%lf %lf %d",&fv, &r, &n);
    printf("The Present Value is %.2lf",PV(r, n, fv));
    return 0;
}

double power(double x, int n){
    double res = 1;
    for(int i=n;i>0;i--){
        res = res * x;
    }
    return res;
}

double PV(double rate, unsigned int nperiods, double FV){
    double PV;
    PV = FV / power((1+rate),nperiods);
    return PV;
}
